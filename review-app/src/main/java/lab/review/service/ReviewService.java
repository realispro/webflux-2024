package lab.review.service;

import lab.review.model.Review;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;

import java.util.ArrayList;
import java.util.List;

@Service
public class ReviewService {

    private final static List<Review> REVIEWS = new ArrayList<>(List.of(
            new Review(1, 1, "That was disaster"),
            new Review(2, 5, "Awesome!"),
            new Review(2, 2, "Booooring"),
            new Review(3, 3, "I've expected more, didn't finish anyway"),
            new Review(3, 5, "So amazing ending")
    ));

    public Flux<Review> getReviews(){
        return Flux.fromIterable(REVIEWS);
    }
}
