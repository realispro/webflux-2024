package lab.review.client;

import lab.review.model.Review;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.netty.http.client.HttpClient;

@Slf4j
public class ReviewClient {

    public static void main(String[] args) {

        WebClient webClient = createWebClient();

        Flux<Review> reviewFlux = webClient
                .get()
                .uri("http://localhost:8080/reviews")
                .retrieve()
                .bodyToFlux(Review.class);

        reviewFlux.subscribe(
                review->log.info("on next review: {}", review),
                t->log.error("on error", t),
                ()->log.info("on complete")
                );

        try {
            Thread.sleep(5_000);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    private static WebClient createWebClient() {
        return WebClient.builder()
                .clientConnector(
                        new ReactorClientHttpConnector(
                                HttpClient.create()
                                        .followRedirect(true)
                                        .secure()
                        )
                )
                .build();
    }
}
