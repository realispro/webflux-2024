package lab.review.web;

import lab.review.model.Review;
import lab.review.service.ReviewService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.util.List;

@RestController
@Slf4j
@RequiredArgsConstructor
public class ReviewController {

    private final ReviewService reviewService;

    @GetMapping("/reviews/last")
    Mono<Review> last(){
        return reviewService.getReviews().last();
    }

    @GetMapping("/reviews/slow")
    Mono<Review> slow(){
        return reviewService.getReviews().last().delayElement(Duration.ofSeconds(3));
    }

    @GetMapping("/reviews")
    Flux<Review> reviews(){
        return reviewService.getReviews().delayElements(Duration.ofSeconds(1));
    }

    @GetMapping("/reviewTexts")
    Flux<String> reviewTexts(){
        return reviewService.getReviews().map(Review::getReviewText);
    }

    @GetMapping("/reviewTextsMono")
    Mono<List<String>> reviewTextsMono(){
        return reviewService.getReviews().map(Review::getReviewText).collectList();
    }

    @GetMapping("/error")
    Flux<Review> error(){
        return Flux.concat(reviewService.getReviews(), Flux.error(new IllegalArgumentException("Boom")));
    }
}
