package lab.review.web;

import lab.review.model.Review;
import lab.review.service.ReviewService;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.*;
import reactor.core.publisher.Mono;

import java.util.Optional;

@Configuration
@RequiredArgsConstructor
public class ReviewRouter {

    private final ReviewService reviewService;

    // TODO: configure router functions:
    // GET /router/reviews - retrieve reviews
    // GET /router/reviews/{id} - retrieve reviews of a movie {id}
    // POST /router/reviews - add review

    @Bean
    RouterFunction<ServerResponse> routerFunction() {
        return RouterFunctions
                .route(RequestPredicates.GET("/router/reviews"), this::retrieveAllReviews)
                .andRoute(RequestPredicates.GET("/router/reviews/{id}"), this::retrieveReviewById)
                .andRoute(RequestPredicates.POST("/router/reviews"), this::addReview);
    }

    private Mono<ServerResponse> retrieveAllReviews(ServerRequest serverRequest){
        return ServerResponse.ok().body(reviewService.getReviews(), Review.class);
    }

    private Mono<ServerResponse> retrieveReviewById(ServerRequest serverRequest){
        return ServerResponse.ok().body(reviewService.getReviews().filter(review -> review.getMovieId() == Integer.parseInt(serverRequest.pathVariable("id"))),
                Review.class);
    }

    private Mono<ServerResponse> addReview(ServerRequest serverRequest) {
        return ServerResponse
                .accepted()
                .body(
                        reviewService.getReviews().concatWith(serverRequest
                                .bodyToMono(Review.class)),
                        Review.class
                );
    }

}
