package lab.emoji;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.netty.http.client.HttpClient;

@SpringBootApplication
public class EmojiApplication {

    public static void main(String[] args) {
        SpringApplication.run(EmojiApplication.class, args);
    }

    @Bean
    WebClient createWebClient() {
        return WebClient.builder()
                .clientConnector(
                        new ReactorClientHttpConnector(
                                HttpClient.create()
                                        .followRedirect(true)
                                        .secure()
                        )
                )
                .build();
    }

}
