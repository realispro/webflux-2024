package lab.emoji.tracker;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.codec.ServerSentEvent;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.time.Duration;
import java.util.Map;
import java.util.stream.Stream;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.springframework.http.MediaType.TEXT_EVENT_STREAM_VALUE;

@RestController
@Slf4j
public class EmojiTrackerStubController {

    private final ObjectMapper objectMapper = new ObjectMapper();

    @GetMapping(value = "/subscribe/eps", produces = TEXT_EVENT_STREAM_VALUE)
    Flux<ServerSentEvent> emojis() {
        return emojiFlux()
                .map(data -> ServerSentEvent.builder(data))
                .map(mapBuilder -> mapBuilder.build());
    }

    Flux<Map> emojiFlux() {
        return Flux.fromStream(()->readLines("/emojis.txt"))
                //.defer(()->Flux.fromStream(readLines("/emojis.txt")))
                .map(this::toJson)
                .repeat()
                .delayElements(Duration.ofSeconds(1))
                .doOnNext(row->log.info("[stub] row emitted: {}", row));
    }



    private Stream<String> readLines(String file) {
        log.info("about to read file {}", file);
        // TODO ensure stream is closed after reading
        return new BufferedReader(new InputStreamReader(getClass().getResourceAsStream(file), UTF_8))
                .lines();
    }

    private Map toJson(String s) {
        try {
            return objectMapper.readValue(s, Map.class);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

}
