package lab.emoji.web;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.codec.ServerSentEvent;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.util.function.Tuples;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.util.Comparator.reverseOrder;
import static java.util.stream.Collectors.toMap;
import static org.springframework.http.MediaType.TEXT_EVENT_STREAM_VALUE;

@RestController
@Slf4j
@RequiredArgsConstructor
public class EmojiController {

    @Value("${emoji-tracker.url}")
    private URI emojiTrackerUrl;

    private final WebClient webClient;

    @GetMapping("/emojis/raw")
    Flux<ServerSentEvent> raw(){

        return retrieveFlux()
                .bodyToFlux(ServerSentEvent.class);
    }

    @GetMapping(value = "/emojis/count", produces = TEXT_EVENT_STREAM_VALUE)
    Flux<Integer> count(){
        return retrieveFlux()
                .bodyToFlux(new ParameterizedTypeReference<Map<String, Integer>>() {})
                .map(map->map.values().stream().mapToInt(Integer::intValue).sum())
                .window(3)
                .flatMap(window->window.reduce((i1, i2)->i1+i2));
    }

    @GetMapping(value = "/emojis/aggregated", produces = TEXT_EVENT_STREAM_VALUE)
    Flux<Map<String, Integer>> aggregated() {
        return retrieveFlux()
                .bodyToFlux(new ParameterizedTypeReference<Map<String, Integer>>() {
                })
                .concatMapIterable(Map::entrySet)
                .scan(new HashMap<>(), (acc, entry) -> {
                    final HashMap<String, Integer> result = new HashMap<>(acc);
                    result.merge(entry.getKey(), entry.getValue(), Integer::sum);
                    return result;
                });
    }

    @GetMapping(value = "/emojis/top", produces = TEXT_EVENT_STREAM_VALUE)
    Flux<Map<String, Integer>> top(){
        return aggregated()
                .map(aggregated->topValues(aggregated, 10));
    }

    @GetMapping(value="/emojis/topStr", produces = TEXT_EVENT_STREAM_VALUE)
    Flux<String> topStr(){
        return top()
                .map(map->keysAsOneString(map));
    }

    private WebClient.ResponseSpec retrieveFlux() {
        return webClient
                .get()
                .uri(emojiTrackerUrl)
                .retrieve();
    }


    // -------------

    private <T> Map<T, Integer> topValues(Map<T, Integer> agg, int n) {
        return new HashMap<>(
                agg.entrySet().stream()
                        .sorted(Map.Entry.comparingByValue(reverseOrder()))
                        .limit(n)
                        .collect(toMap(Map.Entry::getKey, Map.Entry::getValue)));
    }

    String keysAsOneString(Map<String, Integer> m) {
        return m.keySet().stream()
                .map(EmojiController::codeToEmoji)
                .collect(Collectors.joining());
    }

    static String codeToEmoji(String hex) {
        final String[] codes = hex.split("-");
        if (codes.length == 2) {
            return hexToEmoji(codes[0]) + hexToEmoji(codes[1]);
        } else {
            return hexToEmoji(hex);
        }
    }

    private static String hexToEmoji(String hex) {
        return new String(Character.toChars(Integer.parseInt(hex, 16)));
    }



}