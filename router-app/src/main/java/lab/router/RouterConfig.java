package lab.router;

import lombok.Data;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.*;
import reactor.core.publisher.Mono;

@Configuration
public class RouterConfig {

    @Bean
    RouterFunction<ServerResponse> routerFunction() {
        return RouterFunctions
                .route(RequestPredicates.GET("/router/foo/{id}"), this::routerFoo)
                .andRoute(RequestPredicates.GET("/router/bar"), this::routerBar)
                .andRoute(RequestPredicates.POST("/router"), this::routerPost);
    }

    private Mono<ServerResponse> routerFoo(ServerRequest serverRequest){
        return ServerResponse.ok().bodyValue("foo-" + serverRequest.pathVariable("id"));
    }

    private Mono<ServerResponse> routerBar(ServerRequest serverRequest){
        return ServerResponse.ok().bodyValue("bar");
    }

    private Mono<ServerResponse> routerPost(ServerRequest serverRequest){
        return ServerResponse
                .accepted()
                .body(
                        serverRequest
                                .bodyToMono(RouterRequest.class)
                                .map(rr->String.format("%s-%d", rr.getParam1(), rr.getParam2())),
                        String.class
                );
    }

    @Data
    static class RouterRequest{
        private String param1;
        private int param2;
    }

}
