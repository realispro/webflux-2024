CREATE TABLE news (
    id VARCHAR(255) primary key,
    headline VARCHAR(255),
    text VARCHAR(255),
    category VARCHAR(32)
);

INSERT INTO news(id, headline, text, category) VALUES
                                  ('1','Elvis is alive!', 'And he lives in Argentina :)', 'ENTERTAINMENT'),
                                  ('2','Domestos kills coronavirus', 'Professor Trump proves that!', 'SCIENCE'),
                                  ('3','Poland introduces monarchy', 'The King is Max Kolonko I', 'POLITICS'),
                                  ('4','Mammoths found on lonely island', 'And they are happy to meet you', 'SCIENCE'),
                                  ('5','Suprise on Wintar Games!', 'Jamaican bobsled team won competitions!', 'SPORT')

