db.createUser({
    user: 'admin',
    pwd: 'admin',
    roles: [
        {
            role: 'readWrite',
            db: 'news',
        },
    ],
});

db.createCollection('news', {capped: true, size: 100000});
db.createCollection('article', {capped: true, size: 100000});
db.news.insertMany([
    {
        headline: 'Bill Palmer',
        text: 'abc',
        category: 'SPORT'
    },
    {
        headline: 'Lost',
        text: 'again',
        category: 'POLITICS'
    }
]);

db.news.insertMany([
    {
        headline: 'UFO visited Pentagon!',
        text: 'And requested X-files',
        category: 'POLITICS'
    }
]);

db.article.insertMany([
    {
        _id: 'https://www.java-tv.com/2023/09/04/think-about-memory-how-to-write-fast-java-code/',
        _class: 'lab.feed.Article',
        publishedDate: '2023-09-04T16:27:38.000Z',
        title: 'Think About Memory: How to Write Fast Java Code'
    },
    {
        _id: 'https://www.java-tv.com/2023/05/31/frameworks-unplugged-building-distributed-systems-in-pure-java/',
        _class: 'lab.feed.Article',
        publishedDate: '2023-05-31T16:04:08.000Z',
        title: 'Frameworks Unplugged: Building Distributed Systems in Pure Java'
    }
]);
