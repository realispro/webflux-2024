package lab.news.model;


import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class News {

    @Id
    private String id;

    @NotNull
    private Category category;
    @NotNull
    private String headline;
    @NotNull
    private String text;

    public enum Category {
        POLITICS,
        SPORT,
        ENTERTAINMENT,
        SCIENCE
    }
}
