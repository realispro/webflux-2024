package lab.news.dao;

import lab.news.model.News;
import org.springframework.data.mongodb.repository.Tailable;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;

public interface NewsReactiveRepository extends ReactiveCrudRepository<News, String> {

    @Tailable
    Flux<News> getNewsBy();
}
