/*
package lab.news.web;


import jakarta.validation.Valid;
import lab.news.dao.NewsRepository;
import lab.news.model.News;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;


@RestController
@Slf4j
@RequiredArgsConstructor
public class NewsController {

    private final NewsRepository repository;

    @GetMapping("/news")
    List<News> getNews(){
        log.info("get all news");
        return repository.findAll();
    }

    @PostMapping("/news")
    ResponseEntity<?> addNews(@RequestBody News news){
        log.info("about to add news: {}", news);
        news.setId(UUID.randomUUID().toString());
        return ResponseEntity.status(HttpStatus.CREATED).body(repository.save(news));
    }

}
*/
