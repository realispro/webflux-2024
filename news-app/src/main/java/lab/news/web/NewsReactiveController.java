package lab.news.web;

import lab.news.dao.NewsReactiveRepository;
import lab.news.model.News;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.util.UUID;

@RestController
@Slf4j
@RequiredArgsConstructor
@RequestMapping("/reactive")
public class NewsReactiveController {

    private final NewsReactiveRepository newsRepository;

    @GetMapping("/news")
    Flux<News> getNews(){
        log.info("about to retrieve news reactively");

        return newsRepository.getNewsBy()
                //.delayElements(Duration.ofSeconds(2))
                .doOnCancel(()->log.warn("subscription has been cancelled"));
    }

    @PostMapping("/news")
    Mono<News> addNews(@RequestBody News news){
        log.info("about to add news {}", news);
        return Mono.defer(()-> {
            news.setId(UUID.randomUUID().toString());
            return newsRepository.save(news);
        });
    }
}
