package lab.reactor.tasks.operators;

import lab.news.domain.Category;
import lab.news.domain.News;
import reactor.core.publisher.Flux;

public class T16_Merge {

    // TODO Op6:1 Merge flux1 and flux2 values with interleave
    public Flux<News> mergeFluxWithInterleave(Flux<News> flux1, Flux<News> flux2) {
        return null;
    }

    // TODO Op6:2 Merge flux1 and flux2 values with no interleave (flux1 values and then flux2 values)
    public Flux<News> mergeFluxWithNoInterleave(Flux<News> flux1, Flux<News> flux2) {
        return null;
    }

    // TODO Op6:3 Create a Flux of user from Flux of username, firstname and lastname.
    public Flux<News> userNewsFromPartsFlux(Flux<Category> categoryFlux, Flux<String> titleFlux, Flux<String> textFlux) {
        return null;
    }
}
