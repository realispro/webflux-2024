package lab.reactor.tasks.operators;

import lab.news.domain.News;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public class T13_Trigger {

    //TODO Op3:1 Return newStream which is triggered when given stream completes
    public <T> Flux<T> triggerNewStreamWhenStreamCompletes(Flux<?> stream, Flux<T> newStream) {
        return null;
    }

    //TODO Op3:2 Return Mono<Void> which completes when given stream completes
    public Mono<Void> triggerCompleteWhenStreamCompletes(Flux<?> stream) {
        return null;
    }

    //TODO Op3:3 Return Mono<Void> which completes when all given streams complete
    public Mono<Void> triggerCompleteWhenAllStreamsComplete(Flux<?>... streams) {
        return null;
    }

    //TODO Op3:4 Return some entertainment news if given stream is empty
    public Flux<News> returnEntertainmentFluxWhenStreamIsEmpty(Flux<News> flux) {
        return null;
    }
}
