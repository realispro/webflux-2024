package lab.reactor.tasks.operators;

import lab.news.domain.News;
import reactor.core.publisher.Flux;

public class T11_Transform {

    // TODO Op1:1 Capitalize titles
    public Flux<News> capitalizeTitles(Flux<News> flux) {
        return flux;
    }

    //TODO Op1:2 Prefix title with category in format `category: title`
    public Flux<News> prefixTitleWithCategory(Flux<News> flux) {
        return flux;
    }

    //TODO Op1:3 Prefix title with index in format `index: title`
    public Flux<?> prefixTitleWithIndex(Flux<News> flux) {
        return flux;
    }

    //TODO Op1:4 Prefix title with timestamp in format `timestamp: title`
    public Flux<News> prefixTitleWithTimestamp(Flux<News> flux) {
        return flux;
    }

}