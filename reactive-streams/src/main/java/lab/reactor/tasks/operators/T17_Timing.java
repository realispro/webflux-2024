package lab.reactor.tasks.operators;

import lab.news.domain.News;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.util.function.Tuple2;

import java.util.List;

public class T17_Timing {

    // TODO Op7:1 Return the mono which returns its value faster
    Mono<News> getFastestNews(Flux<News> flux1, Flux<News> flux2) {
        return null;
    }

    // TODO Op7:2 Return flux which reports error if no news is signalled for two seconds
    Flux<News> fluxWithTwoSecondsTimeout(Flux<News> flux) {
        return null;
    }

    // TODO Op7:3 Return mono which contains single news emitted after delay in ms
    Mono<News> fluxWithDelayMs(News news, Integer delayMs) {
        return null;
    }

    //TODO Op7:4 create a Flux<News> with items spread over time
    //Items are described by list passed as an argument
    //First field of tuple stores item value
    //Second one - delay of this item in milliseconds
    Flux<News> createFluxFromItemsWithDelaysInMs(List<Tuple2<News, Integer>> itemsWithDelays) {
        return null;
    }

}
