package lab.reactor.tasks.basics;

import lab.news.domain.News;
import reactor.core.publisher.Mono;

public class T02_Mono {

    // TODO Return an empty Mono
    public static Mono<News> emptyMono() {
        return null;
    }

//========================================================================================

    // TODO Return a Mono that never emits any signal
    public static Mono<News> monoWithNoSignal() {
        return null;
    }

//========================================================================================

    // TODO Return a Mono that contains value
    public static Mono<News> valueMono() {
        return null;
    }

//========================================================================================

    // TODO Create a Mono that emits an IllegalStateException
    public static Mono<News> errorMono() {
        return null;
    }
}
