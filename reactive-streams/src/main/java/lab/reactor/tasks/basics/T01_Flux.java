package lab.reactor.tasks.basics;

import lab.news.domain.News;
import reactor.core.publisher.Flux;

import java.util.List;

public class T01_Flux {

    // TODO 1 create empty flux
    public static Flux<News> fluxEmpty(){
        return null;
    }

    // TODO 2 create flux from values
    public static Flux<News> fluxFromValues(News news){
        return null;
    }

    // TODO 3 create flux from list
    public static Flux<News> fluxFromList(List<News> newsList){
        return null;
    }

    // TODO 4 create flux emitting error
    public static Flux<News> fluxEmittingError(){
        return null;
    }

}
