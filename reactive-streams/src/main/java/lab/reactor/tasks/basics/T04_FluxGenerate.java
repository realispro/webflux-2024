package lab.reactor.tasks.basics;

import lab.news.domain.Category;
import lab.news.domain.News;
import lab.utils.ConsoleReader;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;

import java.util.Objects;
import java.util.function.Function;
import java.util.stream.IntStream;

@Slf4j
public class T04_FluxGenerate {

    private static final Function<String, News> NEWS_MAPPER = line -> {
        String[] newsParts = line.split(";");
        Category category = Category.valueOf(newsParts[0]);
        String title = newsParts[1];
        String text = newsParts[2];
        return new News(category, title, text);
    };

    public Flux<News> fluxInfinite(){
        final Flux<News> flux = Flux
                .generate(sink -> {
                    News news = News.POLAND_WON_MUNDIAL;
                    log.debug("Emitting {}", news);
                    sink.next(news);
                });
        //when
        final Flux<News> infiniteNews = flux
                .take(Long.MAX_VALUE);
        //then
        return infiniteNews;
    }
    // TODO 2:2 generate flux of news about mundial repeated 10 times
    public Flux<News> fluxRepeated(){
        return Flux.generate(
                ()->0,
                (counter, sink) -> {
                    if(counter<10){
                        sink.next(News.POLAND_WON_MUNDIAL);
                    } else {
                        sink.complete();
                    }
                    return counter+1;
        });
        //return Flux.range(1, 10).map(i -> News.POLAND_WON_MUNDIAL);
    }


    // TODO 2:3 create flux of interactive news
    public Flux<News> fluxInteractive() {
        ConsoleReader<News> newsConsole = new ConsoleReader<>(NEWS_MAPPER);
        return Flux.create(sink -> {
            News news;
            while (Objects.nonNull(news = newsConsole.readNext())) {
                log.info("emitting news: {}", news);
                sink.next(news);
            }
            sink.complete();
        });
    }

    public static void main(String[] args) {
        T04_FluxGenerate t04FluxGenerate = new T04_FluxGenerate();
        t04FluxGenerate.fluxInteractive()
                .subscribe(
                        n -> log.info("received news: {}", n),
                        t-> log.error("error :)", t),
                        ()->log.info("no more news."));
    }

}
