package lab.reactor.tasks.dev;

import lab.news.domain.Category;
import lab.news.domain.News;
import reactor.core.publisher.Flux;

import java.time.Duration;
import java.util.function.Supplier;

public class T33_StepVerifier {

    // TODO Dev3:1 check that the flux parameter emits POLITICS, next SPORTS elements then completes
    public void expectPoliticsSportComplete(Flux<News> flux) {
    }

    // TODO Dev3:2 check that the flux parameter emits POLITICS, next SPORTS elements then a RuntimeException error.
    public void expectPoliticsSportError(Flux<News> flux) {
    }

    // TODO Dev3:3 Expect 3 elements then complete
    public void expect3Elements(Flux<News> flux) {
    }

}
