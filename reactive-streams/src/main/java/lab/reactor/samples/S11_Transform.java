package lab.reactor.samples;


import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;
import reactor.util.function.Tuple2;

import java.time.Instant;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static java.time.Duration.ofMillis;
import static org.assertj.core.api.Assertions.assertThat;
import static reactor.util.function.Tuples.of;

@Slf4j
public class S11_Transform {

	public static void main(String[] args) throws Exception {
		log.info("Let's transform");

		S11_Transform transform = new S11_Transform();

		transform.mapTransformsItemsOnTheFly();
		transform.mapCanChangeType();
		transform.index();
		transform.timestamp();
		transform.elapsed();

		log.info("done.");
	}


	public void mapTransformsItemsOnTheFly() throws Exception {
		//given
		final Flux<Integer> numbers = Flux.range(5, 4);

		//when
		final Flux<Integer> even = numbers.map(x -> x * 2);

		//then
		List<Integer> list = even.collectList().block();
		assertThat(list).containsExactly(10, 12, 14, 16);
	}

	public void mapCanChangeType() throws Exception {
		//given
		final Flux<String> numbers = Flux.just("Lorem", "ipsum", "dolor", "sit", "amet");

		//when
		final Flux<Integer> lengths = numbers.map(String::length);

		List<Integer> list = lengths.collectList().block();
		assertThat(list).containsExactly(
			"Lorem".length(),
				"ipsum".length(),
				"dolor".length(),
				"sit".length(),
				"amet".length());
	}


	public void index() throws Exception {
		//given
		final Flux<String> strings = Flux.just("A", "B", "C");

		//when
		final Flux<Tuple2<Long, String>> indexed = strings.index();

		//then
		List<Tuple2<Long, String>> list = indexed.collectList().block();
		assertThat(list).containsExactly(
				of(0L, "A"), of(1L, "B"), of(2L, "C"));
	}

	public void timestamp() throws Exception {
		//given
		final Flux<String> ticker = Flux
				.interval(ofMillis(123))
				.map(lng -> "Item-" + lng)
				.take(10);

		//when
		final Flux<Tuple2<Long, String>> stamped = ticker.timestamp();

		//then
		final Flux<Tuple2<Instant, String>> instants = stamped
				.map(tup -> tup.mapT1(Instant::ofEpochMilli));

		instants
				.subscribe(
						x -> log.info("Received {}", x)
				);

		TimeUnit.SECONDS.sleep(4);
	}

	public void elapsed() throws Exception {
		//given
		final Flux<String> ticker = Flux.interval(ofMillis(200))
				.map(lng -> "Item-" + lng);

		//when
		final Flux<Tuple2<Long, String>> elapsedPairs = ticker.elapsed();

		final Flux<Long> elapsed = elapsedPairs.map(Tuple2::getT1)
				.take(5);

		//then
		List<Long> list = elapsed.collectList().block();
		list.forEach(e->assertThat(e).isBetween(180L, 220L));
	}


}
