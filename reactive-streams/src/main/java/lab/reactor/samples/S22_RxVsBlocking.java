package lab.reactor.samples;

import lab.news.domain.News;
import lab.utils.Sleeper;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import java.time.Duration;
import java.util.List;
import java.util.function.Supplier;

@Slf4j
public class S22_RxVsBlocking {

    public static void main(String[] args) {
        log.info("Let's react! ... or block :(");

        S22_RxVsBlocking rxVsBlocking = new S22_RxVsBlocking();
        rxVsBlocking.monoToValue();
        rxVsBlocking.fluxToValues();
        rxVsBlocking.blockingSupplierToFlux();

        log.info("done.");
    }

    public void monoToValue() {
        News news = Mono.just(News.POLAND_WON_MUNDIAL).block();
        log.info("monoToValue: {}", news);
    }

    public void fluxToValues() {
        Iterable<News> iterable = Flux.just(News.POLAND_WON_MUNDIAL, News.OSCARS_CEREMONY).toIterable();
        log.info("fluxToIterable: {}", iterable);
    }

    public void blockingSupplierToFlux() {

        Supplier<List<News>> newsSupplier = () -> {
            Sleeper.sleep(Duration.ofSeconds(5));
            return List.of(News.PANDEMIC_ENDS, News.INFLATION_DROPPED);
        };
        Flux.defer(() -> Flux.fromIterable(newsSupplier.get()))
                .subscribeOn(Schedulers.boundedElastic())
                .subscribe(news -> log.info("rx news: {}", news));

        log.info("converted to rx.");
    }
}
