package lab.reactor;

import lab.news.domain.Category;
import lab.news.domain.News;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

@Slf4j
public class ReactorMain {


    public static void main(String[] args) {

        List<News> newsList = List.of(News.PANDEMIC_ENDS, News.INFLATION_DROPPED, News.POLAND_WON_MUNDIAL, News.OSCARS_CEREMONY);
        var count = newsList.stream()
                .count();
        log.info("there is {} news in a stream", count);

        newsList.stream()
                .filter(news->news.getCategory()== Category.POLITICS)
                .forEach(news->log.info("POLITICS section news: {}" , news));


    }
}
