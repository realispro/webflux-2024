package lab.news.domain;

public enum Category {

    POLITICS,
    SPORT,
    ENTERTAINMENT
}
