package lab.news.plain;

import lab.news.domain.Category;
import lab.news.domain.News;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

// Subject aka Observable aka Publisher
public class Agency {

    private final String name;

    private final List<News> newsList = new ArrayList<>();

    private final List<Reader> observers = new ArrayList<>();

    public Agency(String name) {
        this.name = name;
    }

    public void subscribe(Reader observer){
        observers.add(observer);
    }

    public String getName() {
        return name;
    }

    public News broadcast(Category category, String title, String information){
        News news = new News(category, title, information);
        newsList.add(news);
        observers.forEach(o->{
            o.onNext(news);
        });
        return news;
    }

    public List<News> getNewsList(){
        return Collections.unmodifiableList(newsList);
    }


    @Override
    public String toString() {
        return "Agency{" +
                "name='" + name + '\'' +
                '}';
    }

}
