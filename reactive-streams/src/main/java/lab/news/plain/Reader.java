package lab.news.plain;

import lab.news.domain.News;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Reader {

    public void addSource(Agency agency){
        agency.subscribe(this);
    }


    public void onNext(News news){
        log.info("news update: {}", news);
    }


}
