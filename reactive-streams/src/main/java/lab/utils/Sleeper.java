package lab.utils;

import lombok.extern.slf4j.Slf4j;

import java.time.Duration;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;

@Slf4j
public class Sleeper {
	public static void sleepRandomly(Duration duration) {
		long millis = duration.toMillis();
		double randMillis = millis + ThreadLocalRandom.current().nextGaussian() * millis;
		log.debug("sleeping for {} millis", randMillis);
		sleep(Duration.ofMillis((long) randMillis));
	}
	public static void sleep(Duration duration) {
		try {
			TimeUnit.MILLISECONDS.sleep(duration.toMillis());
		} catch (InterruptedException ignored) {
		}
	}

}
