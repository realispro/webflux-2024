package lab.utils;

import java.util.Scanner;
import java.util.function.Function;

public class ConsoleReader<T> {

    private Scanner scanner = new Scanner(System.in);

    private final Function<String, T> mapper;

    public ConsoleReader(Function<String, T> mapper) {
        this.mapper = mapper;
    }

    public T readNext() {
        String newsText = scanner.next();
        if (newsText.equals("quit")) {
            return null;
        } else {
            return mapper.apply(newsText);
        }
    }
}
